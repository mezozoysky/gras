#
# WARNING: it's not an original CxxTest.cmake from CxxText authors
# This version was modified a bit by Stanislav Demyanovich <mezozoysky@gmail.com>
#
# You can find original CxxText.cmake file in CxxText distribution or You can find it on CxxTest github repo
# https://github.com/CxxTest/cxxtest under the path: build_tools/cmake
#
include("${CMAKE_CURRENT_LIST_DIR}/FindCxxTest.cmake")

function( ay_cxxtest_generate_part argTEST_SOURCE_VAR argTEST_HEADER argNAME )
	get_filename_component( CPP_FILE_NAME ${argTEST_HEADER} NAME )
	string( REGEX REPLACE "h$|hpp$|hxx$" "cpp" CPP_FILE_NAME ${CPP_FILE_NAME} )
	set( CPP_FULL_NAME "${CMAKE_CURRENT_BINARY_DIR}/${CPP_FILE_NAME}" )
	add_custom_command(
			OUTPUT "${CPP_FULL_NAME}"
			COMMAND ${CXXTESTGEN} --part --xunit-printer -w "${argNAME}" -o "${CPP_FULL_NAME}" "${argTEST_HEADER}"
			DEPENDS "${argTEST_HEADER}"
	)
	message( STATUS "${ARAG_NAME} -- CxxTest generate part source: ${CPP_FULL_NAME}" )
	set( ${argTEST_SOURCE_VAR} "${CPP_FULL_NAME}" PARENT_SCOPE )
endfunction( ay_cxxtest_generate_part )

function( ay_cxxtest_generate_root argTESTS_ROOT_VAR argNAME )
	set( ROOT_FILE_NAME "${argNAME}TestsRunner.cpp" )
	set( ROOT_FULL_NAME "${CMAKE_CURRENT_BINARY_DIR}/${ROOT_FILE_NAME}" )
	add_custom_command(
			OUTPUT "${ROOT_FULL_NAME}"
			COMMAND ${CXXTESTGEN} --root --xunit-printer -w "${argNAME}" -o "${ROOT_FULL_NAME}"
	)
	message( STATUS "${ARAG_NAME} -- CxxTest generate root source: ${ROOT_FULL_NAME}" )
	set( ${argTESTS_ROOT_VAR} "${ROOT_FULL_NAME}" PARENT_SCOPE )
endfunction( ay_cxxtest_generate_root )
