// gras
//
// gras - Copyright (C) 2016 Stanislav Demyanovich <stan@angrybubo.com>
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief TypeMap unit-tests
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2016
/// \copyright gras is released under the terms of zlib/png license


#pragma once

#include <cxxtest/TestSuite.h>
#include <gras/typec/TypeMap.hpp>
#include <string>

using gras::typec::TypeMap;

class TypeMapTestSuite : public CxxTest::TestSuite
{
public:
	void test_InsertCorrectness( void )
	{
		using ValueType = std::string;
		TypeMap< ValueType > typeMap;

		typeMap.insert< int >( "integer type" );
		TS_ASSERT_EQUALS( typeMap.find< int >()->second, "integer type" );

		typeMap.insert< bool >( "bool type" );
		TS_ASSERT_EQUALS( typeMap.at< bool >(), "bool type" );

		typeMap.insert< float >( "float type" );
		TS_ASSERT_EQUALS( typeMap.count<float>(), 1 );
		typeMap.insert< float >( "float type again" );
		TS_ASSERT_EQUALS( typeMap.count<float>(), 1 );
		TS_ASSERT_EQUALS( typeMap.at< float >(), "float type" );
	}
};
