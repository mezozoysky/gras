# gras-signal header-only library

set( ARAG_YAS_NAME "Signal" )
set( ARAG_YAS_NSPATH "gras/signal" )
set( ARAG_YAS_TGT "gras-signal-fake" )

set( ARAG_YAS_ROOT ${CMAKE_CURRENT_SOURCE_DIR} )
set( ARAG_YAS_HEADERS_ROOT ${ARAG_YAS_ROOT}/include )
set( ARAG_YAS_SOURCES_ROOT ${ARAG_YAS_ROOT}/src )

set( ARAG_YAS_HEADERS "" )
set( ARAG_YAS_SOURCES "" )

# ===================================================
# ==== ${ARAG_YAS_NAME}/${ARAG_YAS_NSPATH} group ====
# ===================================================
set(GROUP_HEADERS
	${ARAG_YAS_HEADERS_ROOT}/${ARAG_YAS_NSPATH}/Signal.hpp
)
set(GROUP_SOURCES
	${ARAG_YAS_SOURCES_ROOT}/fake.cpp
)
source_group( "${ARAG_YAS_NAME}\\${ARAG_YAS_NSPATH}"
	FILES ${GROUP_HEADERS} ${GROUP_SOURCES}
)
set( ARAG_YAS_HEADERS ${ARAG_YAS_HEADERS} ${GROUP_HEADERS} )
set( ARAG_YAS_SOURCES ${ARAG_YAS_SOURCES} ${GROUP_SOURCES} )



add_library( ${ARAG_YAS_TGT} STATIC
	${ARAG_YAS_HEADERS}
	${ARAG_YAS_SOURCES}
)

set_target_properties( ${ARAG_YAS_TGT} PROPERTIES LINKER_LANGUAGE CXX )

target_include_directories( ${ARAG_YAS_TGT}
	PUBLIC
	$<BUILD_INTERFACE:${ARAG_YAS_HEADERS_ROOT}>
	$<INSTALL_INTERFACE:include>
)

install(
	FILES ${ARAG_YAS_HEADERS}
	DESTINATION include/${ARAG_YAS_NSPATH}
)



if ( ARAG_BUILD_UT )
	add_subdirectory( ut )
endif( ARAG_BUILD_UT )
