// gras
//
// gras - Copyright (C) 2016 Stanislav Demyanovich <stan@angrybubo.com>
//
// This software is provided 'as-is', without any express or
// implied warranty. In no event will the authors be held
// liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute
// it freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but
// is not required.
//
// 2.  Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3.  This notice may not be removed or altered from any
// source distribution.

/// \file
/// \brief Signal unit-tests
/// \author Stanislav Demyanovich <mezozoysky@gmail.com>
/// \date 2016
/// \copyright gras is released under the terms of zlib/png license


#pragma once

#include <cxxtest/TestSuite.h>
#include <gras/signal/Signal.hpp>

using gras::signal::Signal;

class SignalTestSuite : public CxxTest::TestSuite
{
protected:
	static int mVoidSignalCallCount;
	static void onVoidSignal()
	{
		++mVoidSignalCallCount;
	}

public:
	void test_StaticMethodDelageteWithNoParameters( void )
	{
		mVoidSignalCallCount = 0;

		Signal<> sig1{};

		auto connection( sig1.connect( std::bind( &SignalTestSuite::onVoidSignal ) ) );

		TS_ASSERT_EQUALS( mVoidSignalCallCount, 0 );
		sig1();
		TS_ASSERT_EQUALS( mVoidSignalCallCount, 1 );
		sig1();
		sig1();
		TS_ASSERT_EQUALS( mVoidSignalCallCount, 3 );

		sig1.disconnect( connection );
		sig1();
		sig1();
		TS_ASSERT_EQUALS( mVoidSignalCallCount, 3 );
	}

	void test_MethodDelegateWithIntParameter( void )
	{
		struct Observer
		{
			void onSignal1( int i )
			{
				sig1Sum += i;
			}


			int sig1Sum{ 0 };
		};
		Signal< int > sig1{};
		Observer oer{};

		auto connection( sig1.connect( std::bind( &Observer::onSignal1, &oer, std::placeholders::_1 ) ) );

		sig1( 8 );
		sig1( 22 );

		TS_ASSERT_EQUALS( oer.sig1Sum, 30 );

		sig1.disconnect( connection );
		sig1( 5 );

		TS_ASSERT_EQUALS( oer.sig1Sum, 30 );
	}
};

int SignalTestSuite::mVoidSignalCallCount = 0;
