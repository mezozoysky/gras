# gras-sygnal header-only library UT target

set( ARAG_YAS_UT_TGT "${ARAG_YAS_NAME}-ut" )

set( ARAG_YAS_UT_ROOT ${CMAKE_CURRENT_SOURCE_DIR} )
set( ARAG_YAS_UT_HEADERS_ROOT ${ARAG_YAS_ROOT}/ut )
set( ARAG_YAS_UT_SOURCES_ROOT ${ARAG_YAS_ROOT}/ut )

set( ARAG_YAS_UT_HEADERS )
set( ARAG_YAS_UT_SOURCES )

include_directories( ${ARAG_YAS_HEADERS_ROOT} )
include_directories( ${CXXTEST_INCLUDES} )
set( ARAG_YAS_UT_CXXTEST_CASES_ROOT "${ARAG_YAS_UT_ROOT}/cases" )

# ===================================
# ==== ${ARAG_YAS_NAME}/ut group ====
# ===================================
set(GROUP_HEADERS
	# ${ARAG_YAS_UT_HEADERS_ROOT}/Some.hpp
)
set(GROUP_SOURCES
	# ${ARAG_YAS_UT_SOURCES_ROOT}/Some.cpp
)
source_group( "${ARAG_YAS_NAME}\\ut" FILES ${GROUP_HEADERS} ${GROUP_SOURCES} )
set( ARAG_YAS_UT_HEADERS ${ARAG_YAS_UT_HEADERS} ${GROUP_HEADERS} )
set( ARAG_YAS_UT_SOURCES ${ARAG_YAS_UT_SOURCES} ${GROUP_SOURCES} )

set( ARAG_YAS_UT_CXXTEST_CASES_HEADERS )
# =========================================
# ==== ${ARAG_YAS_NAME}/ut/cases group ====
# =========================================
set(GROUP_HEADERS
	${ARAG_YAS_UT_CXXTEST_CASES_ROOT}/SignalTestSuite.hpp
)
source_group( "${ARAG_YAS_NAME}\\ut\\cases" FILES ${GROUP_HEADERS} )
set( ARAG_YAS_UT_CXXTEST_CASES_HEADERS ${GROUP_HEADERS} )


#
# Generate CxxTest cases sources
#

set( ARAG_YAS_UT_CXXTEST_CASES_SOURCES )
foreach( header ${ARAG_YAS_UT_CXXTEST_CASES_HEADERS} )
	set( UT_SOURCE_FILE "" )
	ay_cxxtest_generate_part( UT_SOURCE_FILE "${header}" "${ARAG_YAS_NAME}" )
	set( ARAG_YAS_UT_CXXTEST_CASES_SOURCES ${ARAG_YAS_UT_CXXTEST_CASES_SOURCES} ${UT_SOURCE_FILE} )
endforeach()

set( UT_ROOT_SOURCE_FILE "" )
ay_cxxtest_generate_root( UT_ROOT_SOURCE_FILE "${ARAG_YAS_NAME}" )
set( ARAG_YAS_UT_CXXTEST_CASES_SOURCES ${ARAG_YAS_UT_CXXTEST_CASES_SOURCES} ${UT_ROOT_SOURCE_FILE} )


#
# Build
#

add_executable( ${ARAG_YAS_UT_TGT} WIN32
	${ARAG_YAS_UT_CXXTEST_CASES_HEADERS} ${ARAG_YAS_UT_HEADERS}
	${ARAG_YAS_UT_CXXTEST_CASES_SOURCES} ${ARAG_YAS_UT_SOURCES}
)
set_target_properties( ${ARAG_YAS_UT_TGT} PROPERTIES COMPILE_FLAGS "-Wno-effc++" )
# target_link_libraries( ${ARAG_YAS_UT_TGT}
# 	#<== custom libraries
# 	# custom libraries ==>
# )

install(
	TARGETS ${ARAG_YAS_UT_TGT}
	RUNTIME DESTINATION share/${ARAG_NAME}/bin
)
