# CRY BLUE MURDER
# bash module
#
# author: Stanislav Demyanovich <mezozoysky@gmail.com>
# date: 2016
# license: zlib/png
#
# usage example:
# #!/bin/bash
# . ./uu/cbm.bash
# cbm note "Just a notice."
# cbm warn "Mmm.. waning."

UU_CBM_LEVEL="${UU_CBM_LEVEL:=note}"
UU_CBM_COLORS="${UU_CBM_COLORS:=on}"

#UU_CBM_BLACK='\033[0;30m'
#UU_CBM_RED='\033[0;31m'
#UU_CBM_GREEN='\033[0;32m'
#UU_CBM_BROWN='\033[0;33m'
#UU_CBM_BLUE='\033[0;34m'
#UU_CBM_PURPLE='\033[0;35m'
#UU_CBM_CYAN='\033[0;36m'
#UU_CBM_LIGHT_GREY='\033[0;37m'

#UU_CBM_DARK_GREY='\033[1;30m'
#UU_CBM_LIGHT_RED='\033[1;31m'
#UU_CBM_LIGHT_GREEN='\033[1;32m'
#UU_CBM_YELLOW='\033[1;33m'
#UU_CBM_LIGHT_BLUE='\033[1;34m'
#UU_CBM_LIGHT_PURPLE='\033[1;35m'
#UU_CBM_LIGHT_CYAN='\033[1;36m'
#UU_CBM_WHITE='\033[1;37m'

#UU_CBM_NOCOLOR='\033[0m' # All attributes as by default

UU_CBM_C_D=''
UU_CBM_C_N=''
UU_CBM_C_W=''
UU_CBM_C_E=''
UU_CBM_C_F=''
UU_CBM_C__=''

if [[ "${UU_CBM_COLORS}" == "on" ]]
then
	UU_CBM_C_D='\033[1;36m'
	UU_CBM_C_N='\033[1;37m'
	UU_CBM_C_W='\033[1;33m'
	UU_CBM_C_E='\033[1;35m'
	UU_CBM_C_F='\033[1;31m'
	UU_CBM_C__='\033[0m'
fi

__UU_CBM__LEVELS=( debug note warn error fatal off )

function __uu_cbm__fill_level_index #$1 - var to save result into, $2 - level to find index of
{
	local __RETREF=$1
	local LEVEL=$2
	local INDEX=-1
	for L in "${__UU_CBM__LEVELS[@]}"
	do
		(( INDEX++ ))
		if [[ "$LEVEL" == "$L" ]]
		then
			break
		fi
	done

	eval $__RETREF="'$INDEX'"
}

function cbm #$1 - level, $2 - message
{
#	if [[ "$UU_CBM_LEVEL" == "off" ]]
#	then
#		return 0
#	fi

	local REQ_LEVEL_INDEX=
	__uu_cbm__fill_level_index REQ_LEVEL_INDEX "$1"
	local UU_CBM_LEVEL_INDEX=
	__uu_cbm__fill_level_index UU_CBM_LEVEL_INDEX "$UU_CBM_LEVEL"
	if [[ $REQ_LEVEL_INDEX -lt $UU_CBM_LEVEL_INDEX ]]
	then
		return 0
	fi

	case $1 in
		debug)
			printf "${UU_CBM_C_D}${0##*/} [$1]: $2${UU_CBM_C__}\n"
		;;
		note)
			printf "${UU_CBM_C_N}${0##*/} [$1]: $2${UU_CBM_C__}\n"
		;;
		warn)
			( >&2 printf "${UU_CBM_C_W}${0##*/} [$1]: $2${UU_CBM_C__}\n" )
		;;
		error)
			( >&2 printf "${UU_CBM_C_E}${0##*/} [$1]: $2${UU_CBM_C__}\n" )
		;;
		fatal)
			( >&2 printf "${UU_CBM_C_F}${0##*/} [$1]: $2${UU_CBM_C__}\n" )
		;;
		*)
			printf "${UU_CBM_C_N}${0##*/}: $1${UU_CBM_C__}\n"
		;;
	esac
}

function cbmd
{
	cbm debug "$@"
}

function cbmn
{
	cbm note "$@"
}

function cbmw
{
	cbm warn "$@"
}

function cbme
{
	cbm error "$@"
}

function cbmf
{
	cbm fatal "$@"
}

