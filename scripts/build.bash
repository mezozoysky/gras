#!/bin/bash

SCRIPT_BASE_DIR=$( dirname $( readlink -nf $0 ) ); export SCRIPT_BASE_DIR

UU_CBM_LEVEL="${UU_CBM_LEVEL:=note}"; export UU_CBM_LEVEL
UU_CBM_COLORS="${UU_CBM_COLORS:=on}"; export UU_CBM_COLORS
source "$SCRIPT_BASE_DIR/uu/cbm.bash"

function printUsage
{
	printf "Usage: $( basename $0 ) [options]\n\n"
	printf "options:\n"
	printf "\t--help                   print usage and exit\n"
	printf "\t--produce-filter <list>  list libraries to produce\n"
	printf "\t--custom-defines <defs>  pass more defines to CMake\n"
}

#main

CMAKE_ARGS=

while [[ $# -ne 0 ]]
do
	case $1 in
		--help)
			printUsage
			exit 0
		;;
		--produce-filter)
			shift 1
			CMAKE_ARGS+=" -DAY_PRODUCE_FILTER=$1"
			shift 1
		;;

		--custom-defines)
			shift 1
			CMAKE_ARGS+=" $1"
			shift 1
		;;

		*)
			cbm warn "Unknown option '$1'"
			shift 1
		;;
	esac
done

pushd $SCRIPT_BASE_DIR/..

if [ ! -d build ]
then
	mkdir build #&> /dev/null
fi

if [ ! -d build ]
then
	cbmf "Can't create build directory"
	dirs -c
	exit 8
fi

pushd build

rm -Rf ./* &> /dev/null
cbmn "Running CMake with args: \"${CMAKE_ARGS}\""
cmake $CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=$HOME/dev/local ..
make

popd #build

popd #$SCRIPT_BASE_DIR/..


dirs -c # to be shure
exit 0
